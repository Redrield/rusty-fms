use bitflags::bitflags;

pub mod tags;

use byteorder::{BigEndian, ReadBytesExt};
use crate::Result;

#[derive(Debug)]
pub enum DSTag {

}

#[derive(Debug)]
pub struct UdpStatusPacket {
    pub sequence_number: u16,
    pub comm_version: u8,
    pub status: Status,
    pub team_number: u16,
    pub battery: f32,
    pub tags: Vec<DSTag>
}

impl UdpStatusPacket {
    pub fn decode(mut buf: &[u8]) -> Result<UdpStatusPacket> {
        let seqnum = buf.read_u16::<BigEndian>()?;
        let comm_version = buf.read_u8()?;
        let status = buf.read_u8()?;
        let status = Status::from_bits(status).unwrap_or(Status::empty());
        let team_number = buf.read_u16::<BigEndian>()?;
        let battery = {
            let high = buf.read_u8()?;
            let low = buf.read_u8()?;
            f32::from(high) + f32::from(low) / 256f32
        };

        Ok(UdpStatusPacket {
            sequence_number: seqnum,
            comm_version,
            status,
            team_number,
            battery,
            tags: Vec::new() // Skip for now
        })
    }
}

bitflags! {
    pub struct Status: u8 {
        const ESTOP = 0b10000000;
        const RADIO_COMMS_ACTIVE = 0b00100000;
        const RADIO_PING = 0b00010000;
        const RIO_PING = 0b00001000;
        const ENABLED = 0b00000100;

        const TEST = 0b00000001;
        const AUTO = 0b00000010;
        const TELEOP = 0b0000000;
    }
}