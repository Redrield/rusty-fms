use crate::Result;
use byteorder::{WriteBytesExt, ReadBytesExt, BigEndian};
use crate::proto::common::{AllianceStation, IncomingPacket, OutgoingPacket};
use crate::proto::fms::StationStatus;

pub enum Packet {
    TeamNumber(TeamNumber)
}

pub struct TeamNumber {
    pub team_number: u16,
}

impl IncomingPacket for TeamNumber {
    fn decode(mut buf: &[u8])  -> Result<Self> {
        let team_number = buf.read_u16::<BigEndian>()?;
        Ok(TeamNumber { team_number })
    }
}

pub struct StationInfo {
    station: AllianceStation,
    status: StationStatus
}

impl StationInfo {
    pub fn new(station: AllianceStation, status: StationStatus) -> StationInfo {
        StationInfo {
            station,
            status
        }
    }
}

impl OutgoingPacket for StationInfo {
    fn encode(&self) -> Vec<u8> {
        let mut buf = Vec::new();
        buf.push(0x19);
        buf.push(self.station.as_u8());
        buf.push(self.status as u8);

        let mut f = Vec::with_capacity(buf.len() + 2);
        f.write_u16::<BigEndian>(buf.len() as u16).unwrap();
        f.extend_from_slice(&buf[..]);

        f
    }
}