use bitflags::bitflags;

pub mod tcp;

use super::common::*;

use chrono::prelude::*;
use byteorder::{WriteBytesExt, BigEndian};

#[repr(u8)]
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum StationStatus {
    /// Indicates the driver station is a team in this match and is in the correct player station
    Good = 0,
    /// Indicates the driver station is a team in this match and is in the incorrect player station
    Bad = 1,
    /// Indicates the driver station is not a team in this match
    Waiting = 2,
}

pub struct UdpControlPacket {
    pub sequence_number: u16,
    pub comm_version: u8,
    pub control: Control,
    pub mode: Option<Mode>,
    pub request: u8,
    pub alliance_station: AllianceStation,
    pub tournament_level: TournamentLevel,
    pub match_number: u16,
    pub play_number: u8,
    pub date: Date,
    pub remaining_time: u16,
}

impl UdpControlPacket {
    pub fn new(seqnum: u16, control: Control, mode: Option<Mode>, alliance_station: AllianceStation, tournament_level: TournamentLevel, match_number: u16,
               play_number: u8, remaining_time: u16) -> UdpControlPacket {
        UdpControlPacket {
            sequence_number: seqnum,
            comm_version: 0x0,
            control,
            mode,
            request: 0x0,
            alliance_station,
            tournament_level,
            match_number,
            play_number,
            date: Date::new(),
            remaining_time
        }
    }
}

impl OutgoingPacket for UdpControlPacket {
    fn encode(&self) -> Vec<u8> {
        let mut buf = Vec::new();
        buf.write_u16::<BigEndian>(self.sequence_number).unwrap();
        buf.push(self.comm_version);
        let mut control = self.control.bits();
        if let Some(mode) = self.mode {
            control |= mode.as_u8();
        }
        buf.push(control);
        buf.push(self.request);
        buf.push(self.alliance_station.as_u8());
        buf.push(self.tournament_level.as_u8());
        buf.write_u16::<BigEndian>(self.match_number).unwrap();
        buf.push(self.play_number);
        buf.extend_from_slice(&self.date.encode()[..]);
        buf.write_u16::<BigEndian>(self.remaining_time).unwrap();

        buf
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Date {
    micros: u32,
    second: u8,
    minute: u8,
    hour: u8,
    day: u8,
    month: u8,
    year: u8,
}

impl Date {
    pub fn new() -> Date {
        let local = Utc::now();
        let micros = local.naive_utc().timestamp_subsec_micros();
        let second = local.time().second() as u8;
        let minute = local.time().minute() as u8;
        let hour = local.time().hour() as u8;
        let day = local.date().day() as u8;
        let month = local.date().month0() as u8;
        let year = (local.date().year() - 1900) as u8;

        Date {
            micros,
            second,
            minute,
            hour,
            day,
            month,
            year
        }
    }
}

impl OutgoingPacket for Date {
    fn encode(&self) -> Vec<u8> {
        let mut buf = Vec::new();
        buf.write_u32::<BigEndian>(self.micros).unwrap();
        buf.push(self.second);
        buf.push(self.minute);
        buf.push(self.hour);
        buf.push(self.day);
        buf.push(self.month);
        buf.push(self.year);

        buf
    }
}

bitflags! {
    pub struct Control: u8 {
        const ESTOP = 0b10000000;
        const ENABLED = 0b00000100;
    }
}