//! Module containing data structures common to both FMS and DS packets

use crate::Result;

pub(crate) trait IncomingPacket: Sized {
    fn decode(buf: &[u8]) -> Result<Self>;
}

pub(crate) trait OutgoingPacket {
    fn encode(&self) -> Vec<u8>;
}

/// Represents an alliance station. Values passed to constructors should be in `[1, 3]`
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum AllianceStation {
    Blue(u8),
    Red(u8)
}

impl AllianceStation {
    /// Converts the high level representation into the byte expected by the driver station
    pub fn as_u8(self) -> u8 {
        match self {
            AllianceStation::Red(value) => value - 1,
            AllianceStation::Blue(value) => value + 2
        }
    }
}

/// Represents the possible operating modes the FMS can send to the driver station
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Mode {
    /// Instructs the DS to enable the robot in Test mode
    Test,
    /// Instructs the DS to enable the robot in Autonomous mode
    Autonomous,
    /// Instructs the DS to enable the robot in teleoperated mode
    Teleop,
    /// Sends the same value as Teleop, however is not meant to be enabled. Used as a marker for the auto to teleop transition in a match
    Transition,
}

impl Mode {
    pub fn as_u8(self) -> u8 {
        match self {
            Mode::Test => 0b01,
            Mode::Autonomous => 0b10,
            Mode::Teleop | Mode::Transition => 0b00,
        }
    }

    pub fn from_byte(byte: u8) -> Mode {
        match byte {
            0b01 => Mode::Test,
            0b10 => Mode::Autonomous,
            0b00 => Mode::Teleop,
            _ => panic!()
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum TournamentLevel {
    Test,
    Practice,
    Qualification,
    Elimination
}

impl TournamentLevel {
    pub fn as_u8(self) -> u8 {
        match self {
            TournamentLevel::Test => 0,
            TournamentLevel::Practice => 1,
            TournamentLevel::Qualification => 2,
            TournamentLevel::Elimination => 3,
        }
    }
}
