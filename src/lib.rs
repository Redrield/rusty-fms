pub mod proto;
pub mod fms;
mod state;

pub type Result<T> = std::result::Result<T, failure::Error>;

/// Contains imports for all the things required to start an FMS in code
pub mod prelude {
    pub use crate::fms::{FieldManagementSystem, FMSContext, Match};
    pub use crate::proto::common::{TournamentLevel, Mode, AllianceStation};
    pub use crate::proto::ds::Status;
    pub use crate::state::PlayerStationContext;
}

