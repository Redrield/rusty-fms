use crate::fms::FMSContext;
use crate::Result;
use std::net::{TcpStream, SocketAddr, UdpSocket};
use crate::proto::ds::Status;
use crate::proto::fms::tcp::StationInfo;
use crate::proto::fms::{StationStatus, UdpControlPacket};
use crate::proto::common::OutgoingPacket;
use std::io::Write;
use std::thread;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct PlayerStationContext {
    pub ctx: FMSContext,
    pub status: Status,
}

/// A struct representing a driver station connected to the FMS
pub struct PlayerStation {
    pub team_number: u16,
    pub ctx: FMSContext,
    tcp_connection: TcpStream,
    udp_connection: UdpSocket,
    pub last_seqnum: u16,
    last_status: Status,
    last_time: i64,
}

impl PlayerStation {
    /// Creates a new PlayerStation around the given metadata
    pub fn new(team_number: u16, ctx: FMSContext, addr: SocketAddr, sock: UdpSocket, tcp_connection: TcpStream) -> PlayerStation {
        sock.connect(format!("{}:1121", addr.ip().to_string())).unwrap();

        PlayerStation {
            team_number,
            ctx,
            tcp_connection,
            udp_connection: sock,
            last_seqnum: 1,
            last_status: Status::empty(),
            last_time: -1,
        }
    }

    /// Sends the StationInfo tag over TCP. This finishes the handshake procedure
    /// If the associated context is valid, will send Good. Otherwise, will send Waiting.
    /// Bad is never sent
    pub fn send_station_info(&mut self) -> Result<()> {
        let info = if self.ctx.invalid {
            StationInfo::new(self.ctx.alliance_station, StationStatus::Waiting)
        } else {
            StationInfo::new(self.ctx.alliance_station, StationStatus::Good)
        };

        self.tcp_connection.write(&info.encode()[..])?;
        Ok(())
    }

    /// Sends the given control packet over the associated UDP connection.
    pub fn send_control(&self, packet: UdpControlPacket) -> Result<()> {
        self.udp_connection.send(&packet.encode()[..])?;
        Ok(())
    }

    /// Returns true if this driver station reports as having communication with its roboRIO
    pub fn ready_for_match(&self) -> bool {
        self.last_status.contains(Status::RIO_PING)
    }

    pub fn set_status(&mut self, status: Status) {
        self.last_status = status;
    }

    pub fn to_context(&self) -> PlayerStationContext {
        PlayerStationContext {
            ctx: self.ctx.clone(),
            status: self.last_status.clone()
        }
    }

    pub fn set_last_time(&mut self, time: i64) {
        self.last_time = time;
    }

    pub fn last_time(&self) -> &i64 {
        &self.last_time
    }
}