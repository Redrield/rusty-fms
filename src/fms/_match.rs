use crate::fms::FMSContext;
use crate::proto::common::TournamentLevel;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Match {
    pub contexts: [FMSContext; 6],
    pub tournament_level: TournamentLevel,
    pub match_number: u16,
    pub autonomous_time: f64,
    pub pause_time: f64,
    pub teleop_time: f64,
    pub start_endgame: f64,
}

impl Match {
    pub fn new(contexts: [FMSContext; 6], tournament_level: TournamentLevel, match_number: u16, autonomous_time: f64, pause_time: f64, teleop_time: f64, start_endgame: f64) -> Match {
        Match {
            contexts,
            tournament_level,
            match_number,
            autonomous_time,
            pause_time,
            teleop_time,
            start_endgame
        }
    }
}