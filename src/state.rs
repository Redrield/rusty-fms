use crate::fms::FMSContext;
use crate::proto::common::Mode;

mod ds;

pub use self::ds::*;
use crate::fms::_match::Match;

pub struct FMSState {
    active_connections: Vec<PlayerStation>,
    pub current_match: Option<Match>,
    pub current_mode: Option<Mode>,
    pub remaining_time: f64,
    pub running: bool,
    pub estopped: Vec<u16>,
}

impl FMSState {
    pub fn new() -> FMSState {
        FMSState {
            active_connections: Vec::new(),
            current_match: None,
//            match_contexts: contexts,
//            match_level,
//            match_number,
            current_mode: None,
            remaining_time: 0f64,
            running: false,
            estopped: Vec::new(),
        }
    }

    pub fn connections(&self) -> &Vec<PlayerStation> {
        &self.active_connections
    }

    pub fn connections_mut(&mut self) -> &mut Vec<PlayerStation> {
        &mut self.active_connections
    }

    pub fn find_ctx(&self, team_number: u16) -> Option<FMSContext> {
        match &self.current_match {
            Some(_match) => if let Some(ctx) = _match.contexts.iter().find(|station| station.team_number == team_number) {
                Some(*ctx)
            } else {
                None
            }
            None => None
        }
    }

    pub fn add_connection(&mut self, mut station: PlayerStation) {
        station.send_station_info().unwrap();
        self.active_connections.push(station);
    }

    pub fn load_mode(&mut self, mode: Mode, time: f64) {
        self.current_mode = Some(mode);
        self.remaining_time = time;
    }

    pub fn start_mode(&mut self, mode: Mode, time: f64) {
        self.load_mode(mode, time);
        self.start();
    }

    pub fn load_match(&mut self, _match: Match) {
        self.current_mode = Some(Mode::Autonomous);
        self.current_match = Some(_match);
        self.remaining_time = _match.autonomous_time;
    }

    pub fn clear_match(&mut self) {
        self.current_match = None;
    }

    pub fn start(&mut self) {
        self.estopped.clear();
        self.running = true;
    }

    pub fn estop(&mut self) {
        self.running = false;
        if let Some(_match) = self.current_match {
            for ctx in _match.contexts.iter() {
                self.estopped.push(ctx.team_number);
            }
        }
    }

    pub fn stop_team(&mut self, team: u16) {
        if let Some(_match) = self.current_match {
            if _match.contexts.iter().map(|ctx| ctx.team_number).any(|t| t ==  team) {
                self.estopped.push(team);
            }
        }
    }

    pub fn stop(&mut self) {
        self.running = false;
    }

    pub fn clear_mode(&mut self) {
        self.stop();
        self.estopped.clear();
        self.current_mode = None;
        self.remaining_time = 0f64;
    }

    pub fn match_loaded(&self) -> bool {
        self.current_match.is_some()
    }

    pub fn started(&self) -> bool {
        self.running
    }
}


