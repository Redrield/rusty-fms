use std::net::{UdpSocket, TcpListener};
use std::thread;

use crate::Result;
use crate::state::{FMSState, PlayerStation, PlayerStationContext};
use std::sync::{Arc, Mutex};
use std::io::{ErrorKind, Read, BufReader};
use crate::proto::ds::{UdpStatusPacket, Status};
use crate::proto::fms::tcp::TeamNumber;
use crate::proto::fms::Control;
use byteorder::{ReadBytesExt, BigEndian};
use crate::proto::common::{AllianceStation, TournamentLevel, IncomingPacket, Mode};
use std::sync::atomic::{Ordering, AtomicUsize};
use std::time::{Duration, SystemTime};
use crate::proto::fms::UdpControlPacket;

pub mod _match;

pub use self::_match::Match;
use std::fs::File;
#[cfg(feature = "ears")]
use ears::{Sound, AudioController};
use chrono::Utc;

// Used for enumerating ports for UDP bindings to DS
static NEXT_PORT: AtomicUsize = AtomicUsize::new(1234);

/// Struct core to `fms`. Allows interaction with backend to load match info, control the state of matches,
/// and stop robots if necessary.
pub struct FieldManagementSystem {
    state: Arc<Mutex<FMSState>>,
}

/// Represents a team participating in a match
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct FMSContext {
    pub team_number: u16,
    pub alliance_station: AllianceStation,
    pub invalid: bool,
}

impl FMSContext {
    /// Creates a new valid FMS context with the given team number and alliance station
    pub fn new(team_number: u16, alliance_station: AllianceStation) -> FMSContext {
        FMSContext {
            team_number,
            alliance_station,
            invalid: false,
        }
    }

    /// Creates a new invalid FMS context with the given team number and alliance station
    /// Since the context is invalid, core checks such as whether a match is ready to start
    /// will ignore this context
    pub fn new_invalid(team_number: u16, alliance_station: AllianceStation) -> FMSContext {
        FMSContext {
            team_number,
            alliance_station,
            invalid: true,
        }
    }

    /// Whether the given context is connected to a red alliance station
    pub fn is_red(&self) -> bool {
        match self.alliance_station {
            AllianceStation::Red(_) => true,
            AllianceStation::Blue(_) => false
        }
    }

    /// Whether the current context is valid or not
    pub fn is_valid(&self) -> bool {
        !self.invalid
    }
}


impl FieldManagementSystem {
    /// Initializes a new FieldManagementSystem. This method spawns threads to handle TCP and UDP
    /// connections to any driver stations on the network
    pub fn new() -> Result<FieldManagementSystem> {
        let listener = UdpSocket::bind("0.0.0.0:1160")?;
        listener.set_nonblocking(true).unwrap();
        let fms_state = Arc::new(Mutex::new(FMSState::new()));

        let udp_state = fms_state.clone();
        thread::spawn(move || {
            loop {
                let mut buf = [0u8; 512];
                match listener.recv_from(&mut buf) {
                    Ok(_) => {
                        if let Ok(packet) = UdpStatusPacket::decode(&buf[..]) {
                            let mut state = udp_state.lock().unwrap();
                            if let Some(station) = state.connections_mut().iter_mut().find(|station| station.team_number == packet.team_number) {
                                station.set_status(packet.status);
                                station.set_last_time(Utc::now().timestamp());
                            }
                        }
                    }
                    Err(e) => if e.kind() != ErrorKind::WouldBlock {
                        panic!("{}", e);
                    }
                }

                {
                    let mut state = udp_state.lock().unwrap();
                    // Prune connections we haven't received a status packet from for >=2 seconds
                    state.connections_mut().retain(|station| {
                        let now = Utc::now().timestamp();

                        *station.last_time() == -1 || (now - *station.last_time()) < 5
                    });
                    // something something lifetimes
                    let current_match = state.current_match;
                    let mode = state.current_mode;
                    let running = state.running;
                    let time = state.remaining_time.ceil() as u16;
                    let tournament_level = current_match.map(|_match| _match.tournament_level).unwrap_or(TournamentLevel::Test);
                    let match_number = current_match.map(|_match| _match.match_number).unwrap_or(1);
                    let estopped = state.estopped.clone();
                    for station in state.connections_mut().iter_mut() {
                        let estopped = estopped.contains(&station.team_number);
                        let control = if estopped {
                            Control::ESTOP
                        } else if running {
                            Control::ENABLED
                        } else {
                            Control::empty()
                        };

                        let control = UdpControlPacket::new(station.last_seqnum, control, mode, station.ctx.alliance_station,
                                                            tournament_level, match_number, 0, time);
                        station.send_control(control).unwrap();
                        station.last_seqnum += 1;
                    }

                    // Typically this is only run if robots are enabled, however countdowns should occur
                    // if the mode is Transition
                    if running || mode == Some(Mode::Transition) {
                        if state.remaining_time <= 0.0 {
                            state.stop();
                            // Match timing and mode handling
                            match current_match {
                                Some(_match) => match mode.unwrap() {
                                    Mode::Autonomous => {
                                        state.load_mode(Mode::Transition, _match.pause_time);
                                    }
                                    Mode::Transition => {
                                        #[cfg(feature = "ears")]
                                            thread::spawn(|| {
                                            let mut snd = Sound::new("start-teleop.wav").unwrap();
                                            snd.play();

                                            while snd.is_playing() {}
                                        });
                                        state.start_mode(Mode::Teleop, _match.teleop_time);
                                    }
                                    Mode::Teleop => {
                                        #[cfg(feature = "ears")]
                                            thread::spawn(|| {
                                            let mut snd = Sound::new("match-end.wav").unwrap();
                                            snd.play();
                                            while snd.is_playing() {}
                                        });

                                        state.current_match = None; // Clear the match
                                    }
                                    _ => {}
                                }
                                None => {}
                            }
                        } else {
                            if let Some(_match) = current_match {
                                if state.remaining_time == _match.start_endgame && state.current_mode == Some(Mode::Teleop) {
                                    #[cfg(feature = "ears")]
                                    thread::spawn(|| {
                                        let mut snd = Sound::new("start-endgame.wav").unwrap();
                                        snd.play();

                                        while snd.is_playing() {}
                                    });
                                }
                            }
                            state.remaining_time -= 0.5;
                        }
                    }
                }

                thread::sleep(Duration::from_millis(500));
            }
        });

        let state = fms_state.clone();
        thread::spawn(move || {
            let listener = TcpListener::bind("0.0.0.0:1750").unwrap();

            for sock in listener.incoming() {
                if let Ok(mut sock) = sock {
                    let mut buf = [0u8; 5];
                    sock.read(&mut buf).unwrap();
                    let mut buf = &buf[..];
                    // TeamNumber is a static length, so length header can be discarded
                    buf.read_u16::<BigEndian>().unwrap();
                    if buf.read_u8().unwrap() != 0x18 {
                        println!("Expecting team number. didn't get it");
                    }
                    let tn = TeamNumber::decode(&buf[..]).unwrap();

                    let mut state_guard = state.lock().unwrap();
                    // Only register the new connection if this driver station hasn't connected already
                    if !state_guard.connections().iter().any(|station| station.team_number == tn.team_number) {
                        let ctx = state_guard.find_ctx(tn.team_number).unwrap_or(FMSContext::new_invalid(tn.team_number, AllianceStation::Red(1)));

                        let port = NEXT_PORT.fetch_add(1, Ordering::SeqCst);
                        let udp = UdpSocket::bind(format!("0.0.0.0:{}", port)).unwrap();

                        let station = PlayerStation::new(tn.team_number, ctx, sock.peer_addr().unwrap(), udp, sock);
                        state_guard.add_connection(station);
//                        state.add_connection(sock.peer_addr().unwrap(), ctx, rx, tn.team_number, sock);
                    }
                }
            }
        });

        Ok(FieldManagementSystem {
            state: fms_state,
        })
    }

    /// Whether the current match is ready to begin
    /// Returns true if a match is loaded, and all valid FMS contexts have comms with the FMS and their roboRIO
    pub fn match_ready(&self) -> bool {
        let state = self.state.lock().unwrap();

        if state.current_match.is_none() {
            return false;
        }

        let _match = state.current_match.unwrap();

        let mut required_numbers = _match.contexts.iter().filter(|ctx| ctx.is_valid()).map(|ctx| ctx.team_number);
        let conns = state.connections();

        required_numbers.all(|tn| conns.iter().map(|station| station.team_number).any(|t| t == tn)) &&
            state.connections().iter().all(|station| station.ready_for_match())
    }

    /// Returns all the contexts associated with the loaded match, or an empty vec if no match is loaded
    pub fn contexts(&self) -> Vec<FMSContext> {
        if !self.match_loaded() {
            Vec::new()
        } else {
            self.state.lock().unwrap().current_match.unwrap().contexts.to_vec()
        }
    }

    /// Returns core information about connected player stations.
    pub fn connected_stations(&self) -> Vec<PlayerStationContext> {
        self.state.lock().unwrap().connections().iter().map(|station| station.to_context())
            .collect()
    }

    /// Returns a vec of any teams who aren't ready to start the match
    pub fn unready_teams(&self) -> Vec<u16> {
        let state = self.state.lock().unwrap();

        if state.current_match.is_none() {
            return Vec::new();
        }

        let _match = state.current_match.unwrap();

        let required_numbers = _match.contexts.iter().filter(|ctx| ctx.is_valid()).map(|ctx| ctx.team_number);
        let conns = state.connections().iter().map(|station| station.team_number)
            .collect::<Vec<u16>>();

        required_numbers.filter(|tn| !conns.contains(tn)).collect()
    }

    /// Returns whether the FMS is currently running
    pub fn started(&self) -> bool {
        self.state.lock().unwrap().started()
    }

    /// Loads the given mode with the specified number of seconds. Does not start running
    pub fn load_mode(&self, mode: Mode, time: f64) {
        let mut state = self.state.lock().unwrap();
        state.load_mode(mode, time);
    }

    /// Loads the given mode with the specified number of seconds. Also starts running robots under the new mode
    pub fn start_mode(&self, mode: Mode, time: f64) {
        let mut state = self.state.lock().unwrap();
        state.start_mode(mode, time);
    }

    /// Returns the current loaded mode, or None if none is loaded
    pub fn current_mode(&self) -> Option<Mode> {
        self.state.lock().unwrap().current_mode.clone()
    }

    /// Returns the remaining time in the current mode, or 0
    pub fn remaining_time(&self) -> u16 {
        self.state.lock().unwrap().remaining_time.ceil() as u16
    }

    /// Stops running the match, and sends the estop code to all connected driver stations.
    pub fn estop(&self) {
        self.state.lock().unwrap().estop();
        #[cfg(feature = "ears")]
        thread::spawn(|| {
            let mut snd = Sound::new("match-abort.wav").unwrap();
            snd.play();

            while snd.is_playing() {}
        });
    }

    /// Sends the estop code to the given team
    pub fn estop_team(&self, team: u16) {
        self.state.lock().unwrap().stop_team(team);
    }

    /// Returns true if the match is stopped and every context is emergency stopped
    pub fn aborted(&self) -> bool {
        if !self.match_loaded() {
            return false;
        }

        let mut state = self.state.lock().unwrap();
        let teams: Vec<u16> = state.current_match.unwrap().contexts.iter().map(|ctx| ctx.team_number).collect();
        let stops = state.estopped.clone();

        for team in teams {
            if !stops.contains(&team) {
                return false;
            }
        }
        true
    }

    /// Starts a match cycle
    pub fn start(&self) {
        self.state.lock().unwrap().start();
        #[cfg(feature = "ears")]
        thread::spawn(|| {
            let mut snd = Sound::new("start-auto.wav").unwrap();
            snd.play();
            while snd.is_playing() {}
        });
    }

    /// Stops the FMS
    pub fn stop(&self) {
        self.state.lock().unwrap().stop();
    }

    /// Clears the currently loaded mode
    pub fn clear_mode(&self) {
        self.state.lock().unwrap().clear_mode();
    }

    /// Loads contexts and timings from the given match
    pub fn load_match(&self, _match: Match) {
        self.state.lock().unwrap().load_match(_match);
    }

    /// Clears all match-associated info
    pub fn clear_match(&self) {
        self.state.lock().unwrap().clear_match();
    }

    /// Returns true if a match has been loaded and not cleared
    pub fn match_loaded(&self) -> bool {
        self.state.lock().unwrap().match_loaded()
    }
}

